### Setup
Please make sure that you have ruby 2.3 or higher installed.

```
gem install bundler
```

Once you have Ruby + Bundler installed, you can install the gem dependencies for this exercise with this command:

```
bundle install
```

### To run tests

```
bundle exec rspec
```

### To access application
```
bundle exec ruby job_form.rb
```

