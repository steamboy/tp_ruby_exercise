require 'active_model'

module Job

  class Candidate

    include ActiveModel::Validations

    attr_accessor :first_name, :last_name

    validates :first_name, presence: true
    validates :last_name, presence: true

    def initialize(candidate)
      @candidate = OpenStruct.new(candidate)
      @first_name = @candidate.first_name
      @last_name = @candidate.last_name
    end

    def full_name
      "#{@first_name} #{@last_name}"
    end

  end

end
