require 'lexeme'

module Job

  class Analyze

    def initialize(job_application)
      @job_application = job_application
      @candidate = @job_application.candidate
      @experiences = @job_application.experiences
    end

    def process
      lexer = Lexeme.define do
        token :STOP     =>   /^\.$/
        token :COMA     =>   /^,$/
        token :QUES     =>   /^\?$/
        token :EXCLAM   =>   /^!$/
        token :QUOT     =>   /^"$/
        token :APOS     =>   /^'$/
        token :NUMBER   =>   /^\d+\.?\d?$/
        token :RESERVED =>   /^(gino|cortez|talkpush|)$/
        token :WORD     =>   /^[\w\-]+$/
      end

      candidate = @candidate
      experiences = merge_experiences

      tokens = lexer.analyze do
        from_string "#{candidate.full_name} #{experiences}".downcase
      end

      print "\n"
      puts "LEXICAL ANALYZER RESULTS"
      tokens.each do |t|
        puts "#{t.name}: #{t.value}"
      end
      print "\n"
    end

    private

    def merge_experiences
      merge_experience = ''
      @experiences.each do |experience|
        merge_experience = "#{experience.position} #{experience.company} " + merge_experience
      end
      return merge_experience
    end

  end

end