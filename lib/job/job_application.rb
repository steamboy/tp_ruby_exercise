module Job

  class JobApplication
    include AASM

    aasm do
      state :new_application, initial: true
      state :name_completed
      state :experience_completed

      after_all_transitions :log_status_change

      event :name_complete do
        transitions :from => [:new_application], to: [:name_completed]
      end

      event :experience_complete do
        transitions :from => [:new_application, :name_completed], to: [:experience_completed]
      end
    end

    attr_accessor :candidate, :experiences

    def initialize(candidate)
      @candidate = candidate
      @experiences = []
    end

    def add_experience(experience)
      @experiences << experience
    end

    def log_status_change
      print "\n"
      puts "LOG STATE: Changed from #{aasm.from_state} to #{aasm.to_state} (event: #{aasm.current_event})"
      print "\n"
    end

  end

end