require 'active_model'

module Job

  class Experience

    include ActiveModel::Validations

    attr_accessor :position, :company, :current

    validates :position, presence: true
    validates :company, presence: true

    def initialize(experience)
      @experience = OpenStruct.new(experience)
      @position = @experience.position
      @company = @experience.company
      @current = @experience.current
    end

  end

end