require 'spec_helper'

RSpec.describe Job::JobApplication do
  let(:candidate) { Job::Candidate.new(first_name: "Elgin", last_name: "Cortez") }
  let(:job_application) { Job::JobApplication.new(candidate) }
  let(:experience) { Job::Experience.new(position: "Programmer", company: "ShoutPull", current: true) }

  describe "#candidate" do
    it "gets the job_application candidate" do
      expect(job_application.candidate).to eq(candidate)
    end
  end

  describe "#add_experience" do
    it "can add an experience to the job_application" do
      job_application.add_experience(experience)
      expect(job_application.experiences.first).to eq(experience)
    end
  end

  describe "#experiences" do
    it "gets the job_application experiences" do
      job_application.add_experience(experience)
      expect(job_application.experiences.first).to eq(experience)
    end
  end

  describe "#state" do
    it "gets the initial state of the job_application" do
      expect(job_application).to have_state(:new_application)
    end

    it "should transition to name_completed" do
      expect(job_application).to transition_from(:new_application).to(:name_completed).on_event(:name_complete)
    end

    it "should transition to experience_completed" do
      expect(job_application).to transition_from(:name_completed).to(:experience_completed).on_event(:experience_complete)
    end

  end

end