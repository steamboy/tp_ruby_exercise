require 'spec_helper'

RSpec.describe Job::Experience do
  let(:experience) { Job::Experience.new(position: "Programmer", company: "ShoutPull", current: true) }

  describe "#position" do
    it "gets the position of the experience" do
      expect(experience.position).to eq("Programmer")
    end
  end

  describe "#company" do
    it "gets the company of the experience" do
      expect(experience.company).to eq("ShoutPull")
    end
  end

  describe "#current" do
    it "gets the current of the experience" do
      expect(experience.current).to eq(true)
    end
  end

end