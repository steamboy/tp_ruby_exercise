require 'spec_helper'

RSpec.describe Job::Candidate do
  let(:candidate) { Job::Candidate.new(first_name: "Elgin", last_name: "Cortez") }

  describe "#first_name" do
    it "gets the first_name of the candidate" do
      expect(candidate.first_name).to eq("Elgin")
    end
  end

  describe "#last_name" do
    it "gets the last_name of the candidate" do
      expect(candidate.last_name).to eq("Cortez")
    end
  end

  describe "#full_name" do
    it "gets the full_name of the candidate" do
      expect(candidate.full_name).to eq("Elgin Cortez")
    end
  end

end