require_relative "lib/job"

class JobForm

  def initialize
    @first_name = nil
    @last_name = nil
    @job_application = nil
    @position = nil
    @company = nil
    main_screen
  end

  private

  def main_screen
    clear_scr
    option_one_title = 'Enter Name'
    option_two_title = 'Add Working Experience'
    option_three_title = 'Lexical Analyze Job Application'
    option_four_title = 'Exit'
    hrule
    puts "MAIN MENU"
    if @job_application.nil?
      puts "1. #{option_one_title}"
    else
      puts "1. #{option_one_title} (#{@job_application.candidate.full_name})"
    end
    puts "2. #{option_two_title}"
    puts "3. #{option_three_title}"
    puts "4. #{option_four_title}"
    print "\n"

    display_profile

    if @job_application
      if @job_application.experiences.any?
        puts 'Select: number or "menu name"'
      else
        puts "Select: 2 or '#{option_two_title}'"
      end
    else
      puts "Select: 1 or '#{option_one_title}'"
    end

    selected = gets.chomp.downcase

    if ['1', option_one_title.downcase].include?(selected)
      name_screen
      candidate = Job::Candidate.new(first_name: @first_name, last_name: @last_name)
      if candidate.valid?
        @job_application = Job::JobApplication.new(candidate)
        @job_application.name_complete
      else
        error_handler(candidate)
      end
      main_screen
    elsif ['2', option_two_title.downcase].include?(selected)
      if @job_application.nil?
        print "\n"
        puts "ALERT: Please enter name first"
        print "\n"
        main_screen
      else
        experience_screen
      end
    elsif ['3', option_three_title.downcase].include?(selected)
      Job::Analyze.new(@job_application).process
    elsif ['4', option_four_title.downcase].include?(selected)
      exit
    end

  end

  def name_screen
    hrule
    puts "NAME FORM"
    print "Enter First Name: "
    @first_name = gets.chomp
    print "Enter Last Name: "
    @last_name = gets.chomp
    print "\n"
  end

  def experience_screen
    option_one_title = 'Job Position'
    option_two_title = 'Company'
    option_three_title = 'Back to main menu'

    hrule

    puts "WORK EXPERIENCE MENU"
    puts "1. #{option_one_title}"
    puts "2. #{option_two_title}"
    puts "3. #{option_three_title}"
    print "\n"

    puts 'Select: number or "menu name"'

    selected = gets.chomp.downcase

    if ['1', option_one_title.downcase].include?(selected)
      position_screen
    elsif ['2', option_two_title.downcase].include?(selected)
      company_screen
    elsif ['3', option_three_title.downcase].include?(selected)
      main_screen
    end
  end

  def position_screen
    hrule
    puts "JOB POSITION FORM"
    print "Position Name: "
    @position = gets.chomp
    save_experience
  end

  def company_screen
    hrule
    puts "COMPANY FORM"
    print "Company Name: "
    @company = gets.chomp
    save_experience
  end

  def save_experience
    if @position.nil?
      position_screen
    elsif @company.nil?
      company_screen
    else
      print "\n"
      print "Is this your current working experience [y/n]? "
      answer = gets.chomp
      current = (answer == 'y') ? true : false
      experience = Job::Experience.new(position: @position, company: @company, current: current)
      if experience.valid?
        @job_application.add_experience(experience)
        @job_application.experience_complete if !@job_application.experience_completed?
        @position = nil
        @company = nil
      else
        error_handler(experience)
      end
      print "\n"
      main_screen
    end
  end

  def display_profile
    if @job_application
      hrule
      if @job_application.candidate.present?
        puts "CANDIDATE NAME"
        puts "First Name: #{@job_application.candidate.first_name}"
        puts "Last Name: #{@job_application.candidate.last_name}"
        print "\n"
      end
      if @job_application.experiences.any?
        puts "WORK EXPERIENCE"
        experiences = @job_application.experiences.sort_by{|x| x.current ? 0 : 1}
        experiences.each_with_index do |experience, i|
          puts "#{i + 1})"
          puts "Position: #{experience.position}"
          puts "Company: #{experience.company}"
          puts "(Current)" if experience.current
          print "\n"
        end
      end
    end
  end

  def error_handler(obj)
    print "\n"
    obj.errors.full_messages.each do |error|
      puts "ALERT: #{error}"
    end
    print "\n"
  end

  def hrule
    print ("=" * 50) + "\n\n"
  end

  def clear_scr
    system "clear" or system "cls"
  end

end

JobForm.new